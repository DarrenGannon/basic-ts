export class App {
    
    static async start(argv: {[key: string]: string | boolean | undefined}) {
        console.log('starting')
    }

    static async stop() {
        console.log('stopping')
    }
}