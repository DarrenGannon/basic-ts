require('dotenv').config();
import { App } from './app';
import process from 'process';

(async () => {
    await App.start(argvDistiller())
})().catch(console.error)

function exitHandler() {
    App.stop().then(() => {
        process.exit(0);
    }).catch((err) => {
        console.error(err);
        process.exit(0);
    })
}

process.on('SIGINT', exitHandler);

process.on('beforeExit', exitHandler);

function argvDistiller(): any {
    let argv = process.argv.slice(2)
    let argObj: any = {

    };
    argv.forEach(arg => {
        if(arg.indexOf('=') != -1) {
            let split = arg.split('=')
            argObj[split[0]] = split[1];
        } else {
            argObj[arg] = true;
        }
    })
}