FROM node:14.15.3 AS builder

WORKDIR /app

COPY package*.json ./

RUN npm install

COPY . .

RUN npm run test

RUN npm run build

FROM node:14.15.3-alpine AS runner

WORKDIR /app

COPY --from=builder /app/release ./

RUN npm install --production

ENTRYPOINT ["node"]
CMD ["src/index.js"]