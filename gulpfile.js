var gulp = require('gulp');
var ts = require('gulp-typescript');
var tsProject = ts.createProject('tsconfig.json');
var del = require('del');

gulp.task('typescript', function() {
    return gulp.src(["src/**/*.ts", "!src/**/*.spec.ts"])
        .pipe(tsProject()).js.pipe(gulp.dest('release/src'));
});

gulp.task('env', function() {
    return gulp.src([".env"])
        .pipe(gulp.dest('release'));
});

gulp.task('package', function() {
    return gulp.src(["package*.json"])
        .pipe(gulp.dest('release'));
});

gulp.task('static', function() {
    return gulp.src(["src/**/*.*", "!src/**/*.ts"])
        .pipe(gulp.dest('release/src'));
});

gulp.task('clean', function() {
  return del(['release']);
});

gulp.task("config", gulp.parallel([
    "env",
    "package"
]))

gulp.task('compile', gulp.parallel([
    'typescript',
    'static',
    'config'
]))

gulp.task('default', gulp.series(['clean', 'compile']));